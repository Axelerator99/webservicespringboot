package com.example.springbootovningsuppgift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootOvningsUppgiftApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootOvningsUppgiftApplication.class, args);
    }

}
