package com.example.springbootovningsuppgift;

import lombok.Value;

@Value
public class CreateAnimal {
    String name;
    String binomialName;
    String description;
    String conservationStatus;
}
