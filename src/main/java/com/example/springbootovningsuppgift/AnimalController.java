package com.example.springbootovningsuppgift;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/animals")
public class AnimalController {

    @GetMapping
    public List<Animal> all(){
        return List.of(
                new Animal(UUID.randomUUID().toString(), "Kalle", "Kallus Maximus", "tomt", "tomt"),
                new Animal(UUID.randomUUID().toString(),"Hund", "Canine", "tomt", "tomt"),
                new Animal(UUID.randomUUID().toString(),"Katt", "Feline", "tomt", "tomt")
        );
    }

    @PostMapping
    public Animal createAnimal(@RequestBody CreateAnimal createAnimal){
        return new Animal(UUID.randomUUID().toString(),
                createAnimal.getName(),
                createAnimal.getBinomialName(),
                createAnimal.getDescription(),
                createAnimal.getConservationStatus()
        );
    }

    @GetMapping("/{id}")
    public Animal getAnimal(@PathVariable("id") String id){
        return new Animal(
                id,
                "K",
                "",
                "",
                "");
    }

    @PutMapping("/{id}")
    public Animal update(@RequestBody UpdateAnimal updateAnimal, @PathVariable("id") String id){
        return new Animal(
                id,
                updateAnimal.getName(),
                updateAnimal.getBinomialName(),
                "",
                "");
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id){

    }
}
